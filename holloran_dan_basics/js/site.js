$(document).ready(function(){
	//selector 1
	//Change bg to orange
	var $sl1 = $('ul:first').css('background-color','#FFA500');
	//console log top offset 
	console.log('selector 1 top offset: ' + $sl1.offset().top);
	//console log top position
	console.log('selector 1 top position: ' + $sl1.position().top);

	//selector 2
	//adjust margin-left by 30px 
	$('ul:first li:eq(1)').css('margin-left', '30px');
	
	//selector 3
	//change font color to red
	$('ul:first li:eq(3)').css('color', '#ff0000');
	//console log selector 3 width
	console.log( 'selector 3 width: ' + 
				 $('ul:first li')
				 .filter('li:last')
				 .width() 
	);
	
	//selector 4
	var $sl4 = $('h3').last();
	//change float to right
	$sl4.css('float', 'right');
	//Add "listhead" class
	$sl4.addClass("listhead");
	//Check "listheadclass"
	console.log('class "listhead" add to h3 = ' + $sl4.hasClass("listhead"));
	
	//selector 5
	//change last 3 anchor tag links to http://www.notgoogle.com
	$('ul:last li a').not('a:first').attr('href', 'http://www.notgoogle.com');
});