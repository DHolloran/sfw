$(function() {
	
	//Variables
	var $navLeft = $('ul:first');
	var $navRight = $('ul:last');
	
	//Create 10 <li></li>
	for (var i = 0  ; i<10  ;  i++){
	$('<li><a href="#">'+i+'</a></li>').appendTo($navLeft);
	}
	
	//change a tag color:#e50 and font-size:14px
	//clone #navLeft to #navRight
	$($navLeft)
	.children()
	.has('a')
	.css({
			'color' : '#e50','font-size' : '14px'})
	.clone()
	.appendTo($navRight);
									
	//change #navRight a tags font-size:30px
	$navRight.children().has('a').css('font-size', '30px');
	
	
});

