$(function() { 
  //Drop Down Menu
  (function(){
  	
  	//Init Vars
  	var $subMenu = $('.sublinks'),  
		 	 $mainMenu = $('.mainlinks'),
		 	 $timer
		 ;

	  //Bind mouseenter to .dropdown	
	  $('.dropdown').bind('mouseenter',function(){  
		 $subMenu.hide();//Make sure .submenu is hidden  
		 clearTimeout($timer);//Clear timeout from mouseleave
		 
		 //Set timeout 
		 $timer = setTimeout(function(){
		 	//show .submenu
		 	$subMenu.show().css({  
			  	position:'absolute',  
			  	top: $mainMenu.offset().top + $mainMenu.height() + 10,  
			  	left: $mainMenu.offset().left + 10,  
			  	zIndex:1000  
			  });
			    
		 },500);  	 
		    
	  });
	  
	  $mainMenu.bind('mouseleave',function(){  
		 	clearTimeout($timer); //Clear timeout from mouseenter
		 	
		 	//Set timeout and show .submenu
		 	$timer = setTimeout(function(){
		 		$subMenu.hide();
		 	},200);
		 	  
	  });   

  })();
//End Drop Down Menu
  
//Start Tabs Section
  (function(){
  	//Init Vars
  	var $mainTab = $('#mainTab'),
  		$infoTab = $('#infoTab'),
  		$storiesTab = $('#storiesTab'),
  		$main = $('#mainTxt'),
  		$info = $('#infoTxt'),
  		$stories = $('#storiesTxt')
  	; 
  	
  	//Reset all text
  	function resetTxt(){
  		$main.hide();
  		$stories.hide();
  		$info.hide();
  	};
  	
  	//Sets Maintab as starting default
	(function init(){
		$mainTab.css({
  			backgroundColor: '#6cd5f7'
  		});
  		
  		$infoTab.css({
  			backgroundColor: '#CCCCCC'
  		});
  		
  		$storiesTab.css({
  			backgroundColor: '#CCCCCC'
  		});

		$main.show();//Show main text  	
	})();
	
  	//Main Tab
  	$mainTab.bind('click', function(){
  		
  		resetTxt();
  		
  		$mainTab.css({
  			backgroundColor: '#6cd5f7'
  		});
  		
  		$infoTab.css({
  			backgroundColor: '#CCCCCC'
  		});
  		
  		$storiesTab.css({
  			backgroundColor: '#CCCCCC'
  		});

		$main.show();//Show main text
  	});
  	
  	//Info Tab
  	$infoTab.bind('click', function(){
		
		resetTxt();
		
		//Set tabs bg color
		$mainTab.css({
  			backgroundColor: '#CCCCCC'
  		});
  		
  		$infoTab.css({
  			backgroundColor: '#6cd5f7'
  		});
  		
  		$storiesTab.css({
  			backgroundColor: '#CCCCCC'
  		});
		$info.show(); //Show info text
  		
  	});
  	
  	//Stories Tab
  	$storiesTab.bind('click', function(){
  		
  		resetTxt();
  		
  		//Set tabs bg color
		$mainTab.css({
  			backgroundColor: '#CCCCCC'
  		});
  		
  		$infoTab.css({
  			backgroundColor: '#CCCCCC'
  		});
  		
  		$storiesTab.css({
  			backgroundColor: '#6cd5f7'
  		});
  		
  		$stories.show(); //Show stories text
  		
  	});
  })();
  
  //End Tabs Section
  	
}); 