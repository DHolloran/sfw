$(function(){
	//Global Vars
    var cart = $('#scart'),
        cartDrop = $('div.cartdropbox'),
        item = $('.photo'),
        prodID,
        itemName,
        price,
        emptyCart = $('.cartitem.cf.emptycart'),
        cartItems = $('.cartitems.cf'),
		clearCart = $('div#clearcart a'),
		inCart = [],
		item1Amount = 0,
		item2Amount = 0,
        item3Amount = 0,
        item4Amount = 0,
        item5Amount = 0,
        trashCan = $('.ui-icon.ui-icon-trash a'),
        cartItem = $('.cartitem'),
        itemHeight = cartItem.outerHeight;
    ;
    
    //jsfollow plugin for cart
    $('#scart').jfollow('#cartfollow', 20);
    
    //Drag selection
    item.draggable({ helper: function(event){
                       var that = $(this); 
						
                        prodID = that.parent().attr('rel');
                        price = that.siblings().find('.listprice').html();
                        itemName = that.siblings().find('h4').html();
						return $('<img src="images/icons-small/jquery.jpg" alt="" width="32" height="32" class="ui-widget-header"/>' )
                        .css({
                            zIndex: 1005    
                        });
                     },
                     cursorAt: {cursor: "crosshair", top: -5, left: -5 }
    });    
    
    //Drop selection in cart
    cartDrop.droppable({
       drop: function(event, ui){
            emptyCart.remove();
            addItem();
       }
    });
    
	//Remove single items
	function removeOne(){
		var that = $(this),
			id = that.attr('id');
			index = $.inArray(id, inCart)
		;
		
		that.effect('explode', function(){
			if( id == 0001){
			    item1Amount = 0;
			}else if( id == 0002 ){
			    item2Amount = 0;
			}else if( prodID == 0003 ){
			    item3Amount = 0;
			}else if( prodID == 0004 ){
			    item4Amount = 0;
			}else if( prodID == 0005 ){
			    item5Amount = 0;
			};

			that.empty();
			inCart.splice(index,1);
				
			if(inCart.length == 0){
				$('<div class="cartitem cf emptycart" rel="empty">' +
				'<span class="title">Your cart is currently empty.</span>' +
				'</div>').appendTo(cartItems);
			};
		});		


		return false;
	}
	
	//append item to cart if not already there
	function appendItem(){
		$('<div class="cartitem" id="' + prodID + '">' +
            '<span class="ui-state-default trashitem">' +
                '<span class="ui-icon ui-icon-trash"><a href="#"></a></span>' +
            '</span>' +
            '<span class="title">' + itemName + '</span>' +
            '<input type="text" class="amount" value="0" />' +
            '<span class="price">' + price + '</span>'+
            '<div class="clear"></div>' +
         '</div>').appendTo(cartItems).bind('click', removeOne);
		
	}
	
    //Set items quantity in the cart
	function changeItemQuantity(){
		var id = '#'+prodID,		
			cartValue = cartItems.find(id).find('.amount')
		;
		
        if( prodID == 0001){
            item1Amount++;
            cartValue.attr('value', item1Amount);
        }else if( prodID == 0002 ){
            item2Amount++;
            cartValue.attr('value', item2Amount);
        }else if( prodID == 0003 ){
            item3Amount++;
            cartValue.attr('value', item3Amount);
        }else if( prodID == 0004 ){
            item4Amount++;
            cartValue.attr('value', item4Amount);
        }else if( prodID == 0005 ){
            item5Amount++;
            cartValue.attr('value', item5Amount);
        };
    
	};
	
    //add item to cart
    function addItem(){
    
		if(inCart.length != 0){
			if($.inArray(prodID, inCart) === -1){
				inCart.push(prodID);
				appendItem();
				changeItemQuantity();
			}else{
				changeItemQuantity();
			};	
			
		}else{
			inCart.push(prodID);
			cartItems.empty();
			appendItem();
            changeItemQuantity();
		};	
        
    };

    //Remove all items and set quantities back to 0
    function removeAll(){
            cartItems.empty();              
            item1Amount = 0;
            item2Amount = 0;
            item3Amount = 0;
            item4Amount = 0;
            item5Amount = 0;
			$(  '<div class="cartitem cf emptycart" rel="empty">' +
			    '<span class="title">Your cart is currently empty.</span>' +
				'</div>').appendTo(cartItems);
			cartItems.animate({ opacity: 1,
								height: '100%'},
								2000);
			inCart.splice( 0, inCart.length );
    }
    
	//Remove all items
	clearCart.bind('click', function(){
        if(inCart.length >=1){
                
                $( '#modal' ).dialog({
                    title: 'Empty Cart',
                    resizable: false,
                    modal: true,
                    position: 'top',
                    closeOnEscape: true,
                    buttons:{
                    "Ok" : function(){
                            cartItems.animate({opacity: 0,
                                                height: 0}, 1000, removeAll);
                            $(this).dialog("close");
                    },
                    "Cancel" : function(){
                        $(this).dialog("close");
                    }
            }});
        };
		return false;
	});
});