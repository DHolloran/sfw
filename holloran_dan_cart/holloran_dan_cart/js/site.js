$(function(){
	//Global Vars
    var cart = $('#scart'),
        cartDrop = $('div.cartdropbox'),
        item = $('.productitem.cf'),
        prodID,
        itemName,
        price,
        emptyCart = $('.cartitem.cf.emptycart'),
        cartItems = $('.cartitems.cf'),
		clearCart = $('div#clearcart a'),
		inCart = [],
		itemAmount = 0,
		item0001Amount = 0,
		item0002Amount = 0,
		trashCan = $('.ui-icon.ui-icon-trash')
    ;
    
    //jsfollow plugin for cart
    $('#scart').jfollow('#cartfollow', 20);
    
    //Drag selection
    item.draggable({ helper: function(event){
                       var that = $(this); 
						
                        prodID = that.attr('rel');
                        price = that.find('.listprice').html();
                        itemName = that.find('h4').html();
                        
						return $('<img src="images/icons-small/jquery.jpg" alt="" class="ui-widget-header"/>' )
                        .css({
                            zIndex: 1005    
                        });
                     },
                     cursorAt: {cursor: "crosshair", top: -5, left: -5 }
    });    
    
    //Drop selection in cart
    cartDrop.droppable({
       drop: function(event, ui){
            emptyCart.remove();
            addItem();
			console.log('dropped!');
       }
    });
    
	//append item to cart if not already there
	function appendItem(){
		
		
		$('<div class="cartitem" id="' + prodID + '">' +
            '<span class="ui-state-default trashitem">' +
                '<span class="ui-icon ui-icon-trash"></span>' +
            '</span>' +
            '<span class="title">' + itemName + '</span>' +
            '<input type="text" class="amount" value="1" />' +
            '<span class="price">' + price + '</span>'+
            '<div class="clear"></div>' +
         '</div>').appendTo(cartItems);
		
	}
	
//NEEDS MORE WORK!!!!!!!!!!!!!!!!!!!!!	
    //add item to cart
	function changeItemQuantity(){
		if(prodID === 0001){
					itemAmount = item0001Amount;
					console.log(itemAmount);
				}else if(prodID === 0002){
					itemAmount = item0002Amount;
					console.log(itemAmount);
				}
				
				var id = '#'+prodID;
				
				var cartValue = cartItems.find(id).find('.amount');
				itemAmount++
				cartValue.attr('value', itemAmount);
	}
	
    function addItem(){
    
		if(inCart.length != 0){
			if($.inArray(prodID, inCart) === -1){
				inCart.push(prodID);
				appendItem();
				changeItemQuantity();
			}else{
				changeItemQuantity();	
			}	
			
		}else{
			inCart.push(prodID);
			cartItems.empty();
			appendItem();
		}	
        
    }
	
	//Remove single items
	trashCan.bind('click', function removeSingleItem(){
								console.log('empty');
						    });
	
	//Remove all items
	clearCart.bind('click', function removeAllItems(){
								cartItems.empty();
								$('<div class="cartitem cf emptycart" rel="empty"><span class="title">Your cart is currently empty.</span></div>').appendTo(cartItems)
								
								inCart.splice(0,inCart.length);
						
							});
});