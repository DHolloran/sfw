$(function(){
	//Global Variables
	var searchtext = $('#searchtext'),
		submitbtn = $('#searchsubmit'),
		searchresults = $('#searchresults'),
		resultslist = searchresults.find('ul'),
		inputvalue="",
        searchDB,
		indexUp = -1, 
		indexDown = 0,
		urlindex = 0,
		upActive = false,
		downActive = false
	;
	
	//initalize
	(function(){
		searchresults.hide();
		searchtext.focus();
		searchtext.val('');
	})();
    
    //Seach video database
    searchDB = function(){
        if(inputvalue.length > 0){
			$.ajax({
			   url: 'xhr/vidsearch.php',
			   type: 'GET',
			   dataType: 'json',
			   data: {q: inputvalue},
			   success: function(response){
					//Empty results list
					resultslist.empty();
					//Get results list
					if(response.length >1){
					//Build HTML if any results are found
						for( var i=0, j=response.length; i<j; i++ ){
							
							$(  '<li id="'+ i +'" rel="'+ response[i].url+'">'+
									'<img src="images/icons-small/'+ response[i].icon +'" />' +
									'<span>'+ response[i].title +'</span>' +
								'</li>')
							.appendTo(resultslist);
						}
                        resultslist.delegate('li','click', function(){
								window.location = url;	
							});
					}else{
						//add no results if nothing to display
						$( '<li>No results to display</li>')
							.appendTo(resultslist)
							.css({
								fontSize: '18px',
								textAlign: 'center',
								color: '#666666'
							});	
					}
				},
				error: function(j,t,e){
					
				}
			});
			
			//Show results
			searchresults.show();
		}        
    };
    
    //Search box focusin
    searchtext.bind('focusin', function(){
        var that = $(this);
        that.val('');
        
		return false;
    });
    
    //Search box focusout
    searchtext.bind('focusout', function(){
        var that = $(this);
		
		//Fade search results out
		searchresults.fadeOut(1000, function(){
			searchresults.hide();
		});
		
		//Reset initial values
		inputvalue = '';
		indexDown = 0;
		indexUp = -1;
        that.val('search...');
		
        return false;
    });
    
	//Submit button
	submitbtn.bind('click', function(e){
		e.preventDefault();
	});
	
    //Reset selections style
   var resetstyle = function(r){
    //Reset all backgrounds and font colors
		for(var i=0, j=r;  i<j ; i++ ){
			$('#'+i)
			.css({
				backgroundColor: '#ffffff',
				color: '#333'
			});
		}
    };
	
    //Search box keyup
	searchtext.bind('keyup' ,function(e){	
		var that = $(this),
			resultslength = resultslist.children('li').length;
		;

        if(e.which == 38){
        //Up arrow key
			//check which way selection is moving
			if(downActive){
				indexUp = indexDown;
				indexUp-=2;
				downActive = false;
			}else{
				upActive = true;
			}
			
			//constrict index up between 0 and results array length
			if(indexUp > resultslength){
				indexUp = 0;
			}else if(indexUp < 0){
				indexUp = resultslength -1;
			}

			//reset all background colors and font colors
			resetstyle(resultslength);
			
			//set selection's background to #31b8ea and font color to #fff
			$('#'+indexUp)
			.css({
				backgroundColor:'#31b8ea',
				color: '#fff'
			});
			
			//set index for url links
			urlindex = indexUp;
			
			//decrement index by one
			indexUp--;
			
        }else if(e.which == 40){
            //Down arrow key
			
			//Check if up value is active 
			if(upActive){
				indexDown = indexUp;
				indexDown+=2;
				upActive = false;

			}else{
				downActive = true;
			}
			
			//constrict index between 0 and results array length
			if(indexDown > resultslength){
				
				indexDown = 0;
				
			}else if(indexDown < 0){
				
				indexDown = resultslength-1;
			
			}
			
			//reset all background colors and font colors
			resetstyle(resultslength);
			
			//Set active result's background to #31b8ea and font color to #fff
			$('#'+indexDown)
			.css({
				backgroundColor:'#31b8ea',
				color: '#ffffff'
			});
			
			//set index for url links
			urlindex = indexDown;
			
			//increment index by one
			indexDown++;
			
        }else if(e.which == 13){
                //Enter key
            var url = $('#'+urlindex).attr('rel');
               window.location = url;
        }else{
            //If input>2 search db and show results
            //set input value
            inputvalue = that.val();
                
            //wait until after 2 keyups to show results
            if( inputvalue.length < 2){
                searchresults.hide();
            }else if( inputvalue.length >= 2 ){
                searchDB();
                searchresults.show();
            }
    
        }

        return false;
    });
	
});